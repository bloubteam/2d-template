using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class Player : MonoBehaviour
{
    [SerializeField]
    LayerMask layerMask;

    [SerializeField]
    Collider2D faceCollider;

    [SerializeField]
    Collider2D floorCollider;

    [ReadOnly, SerializeField]
    float speed = 0;

    [SerializeField]
    float minWalkSpeed = 1.5f;

    [SerializeField]
    float maxWalkSpeed = 3f;

    [SerializeField]
    float runSpeed = 5;

    [SerializeField]
    float timeBeforeRun = 2;

    [ReadOnly, SerializeField]
    float walkTime = 0;

    [SerializeField]
    Health health;

    [SerializeField]
    bool controlEnabled = true;

    [SerializeField]
    float jumpHeight = 10f;

    Rigidbody2D r2d;

    Animator animator;

    [Inject]
    readonly SignalBus signalBus;

    public void disableControls()
    {
        controlEnabled = false;
    }
    public void enableControls()
    {
        controlEnabled = true;
    }
    void Start()
    {
        r2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    Vector2 moveButton = Vector2.zero;
    public void onMove(InputAction.CallbackContext context)
    {
        moveButton = context.ReadValue<Vector2>();
    }

    bool jumpButton = false;
    public void onJump(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            jumpButton = true;
        }
        if (context.canceled)
        {
            jumpButton = false;
        }
    }

    public void OnDie()
    {
        signalBus.Fire(new SpawnPlayer(gameObject));
    }

    void FixedUpdate()
    {
        bool isGrounded = floorCollider.IsTouchingLayers(layerMask);
        if (controlEnabled)
        {
            if (moveButton.x > 0.01f)
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 0, transform.rotation.eulerAngles.z);
            else if (moveButton.x < -0.01f)
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, 180, transform.rotation.eulerAngles.z);
            if (isGrounded)
                walkTime += Time.fixedDeltaTime;
            speed = Mathf.Lerp(minWalkSpeed, maxWalkSpeed, walkTime / timeBeforeRun);
            if (walkTime >= timeBeforeRun)
                speed = runSpeed;
            if (moveButton.x == 0)
            {
                walkTime = 0;
                speed = 0;
            }
            r2d.velocity = new Vector2(Mathf.Sign(moveButton.x) * speed, r2d.velocity.y);

            if (faceCollider.IsTouchingLayers(layerMask))
                r2d.velocity = new Vector2(0, r2d.velocity.y);

            if (jumpButton && isGrounded)
            {
                r2d.velocity = new Vector2(r2d.velocity.x, jumpHeight);
                animator.SetTrigger("Jump");
            }
        }

        animator.SetBool("Falling", r2d.velocity.y < -0.01f && !isGrounded);
        animator.SetFloat("Speed", Mathf.Abs(r2d.velocity.x));
        animator.SetFloat("WalkSpeedMultiplier", speed / minWalkSpeed);
    }
}
