using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class Tank : MonoBehaviour
{
    [SerializeField]
    Animator leftTrack;

    [SerializeField]
    Animator rightTrack;

    [SerializeField, Range(0.0f, 1.0f)]
    float trackSpeedChange = 0.5f;

    [SerializeField]
    float maxAngleSpeed = 90;

    Rigidbody2D r2d;

    [Inject]
    readonly SignalBus signalBus;

    void Start()
    {
        r2d = GetComponent<Rigidbody2D>();
    }

    Vector2 moveButton = Vector2.zero;
    public void onMove(InputAction.CallbackContext context)
    {
        moveButton = context.ReadValue<Vector2>();
    }
    public void OnDie()
    {
        signalBus.Fire(new SpawnPlayer(gameObject));
    }

    void FixedUpdate()
    {
        if (moveButton != Vector2.zero)
        {
            float speed = 1;
            r2d.velocity = transform.up * speed;
            float angle = Mathf.Atan2(moveButton.y, moveButton.x) * Mathf.Rad2Deg - 90;
            float deltaAngle = Mathf.DeltaAngle(r2d.rotation, angle);
            if (Mathf.Abs(deltaAngle) > maxAngleSpeed * Time.fixedDeltaTime)
                deltaAngle = Mathf.Sign(deltaAngle) * maxAngleSpeed * Time.fixedDeltaTime;
            r2d.MoveRotation(Mathf.Repeat(r2d.rotation + deltaAngle, 360));
            Debug.Log(r2d.rotation);
            if (Mathf.Abs(deltaAngle) > 0.001f)
            {
                leftTrack.SetFloat("Speed", speed * (1 - (Mathf.Sign(deltaAngle) * trackSpeedChange)));
                rightTrack.SetFloat("Speed", speed * (1 + (Mathf.Sign(deltaAngle) * trackSpeedChange)));
            }
            else
            {
                leftTrack.SetFloat("Speed", speed);
                rightTrack.SetFloat("Speed", speed);
            }
        }
        else
        {
            leftTrack.SetFloat("Speed", r2d.velocity.magnitude);
            rightTrack.SetFloat("Speed", r2d.velocity.magnitude);
        }
    }
}
