using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PrefabTile))]
public class PrefabTileEditor : Editor
{
    SerializedProperty prefab;
    SerializedProperty tileSprite;

    void OnEnable()
    {
        prefab = serializedObject.FindProperty("prefab");
        tileSprite = serializedObject.FindProperty("tileSprite");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(prefab);
        EditorGUILayout.PropertyField(tileSprite);
        serializedObject.ApplyModifiedProperties();
    }
}