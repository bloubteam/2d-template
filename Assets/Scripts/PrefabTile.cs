using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PrefabTile : Tile
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private Sprite tileSprite;

    private List<Vector3Int> alreadyRefreshed = new List<Vector3Int>();

    public override void GetTileData(Vector3Int location, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(location, tilemap, ref tileData);
        if (Application.isPlaying)
            tileData.sprite = null;
        else
            tileData.sprite = tileSprite;
    }

    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject tileGO)
    {
        if (Application.isPlaying)
        {
            if (!alreadyRefreshed.Contains(position))
            {
                alreadyRefreshed.Add(position);
                tilemap.RefreshTile(position);
            }
        }
        else
        {
            alreadyRefreshed.Clear();
        }

        GOTileMap GOTileMap = tilemap.GetComponent<GOTileMap>();
        if (!GOTileMap)
        {
            GOTileMap = tilemap.GetComponent<Transform>().gameObject.AddComponent<GOTileMap>();
        }

        GOTileMap.Spawn(prefab, position, tilemap.GetTransformMatrix(position).rotation, tilemap.GetTile(position));
        return true;
    }

#if UNITY_EDITOR
    [MenuItem("Assets/Create/2D/Tiles/Prefab Tile")]
    public static void Create()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Prefab Tile", "New prefab tile", "asset", "Save prefab tile", "Assets");
        if (path == "")
            return;

        PrefabTile prefabTile = CreateInstance<PrefabTile>();
        prefabTile.flags = TileFlags.LockAll;
        prefabTile.gameObject = null;
        prefabTile.colliderType = ColliderType.None;
        AssetDatabase.CreateAsset(prefabTile, path);
    }
#endif
}
