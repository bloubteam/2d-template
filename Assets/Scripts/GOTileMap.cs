using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[ExecuteInEditMode]
[RequireComponent(typeof(Tilemap))]
public class GOTileMap : MonoBehaviour
{
    [SerializeField, HideInInspector]
    private List<GOTile> prefabs = new List<GOTile>();
    private Tilemap tilemap;
    private Vector3 anchor = new Vector3(0.5f, 0.5f, 0);

    [System.Serializable]
    public class GOTile
    {
        public GameObject Prefab;
        public GameObject Instance;
        public Vector3Int Position;
        public TileBase TileBase;

        public GOTile(GameObject prefab, GameObject instance, Vector3Int position, TileBase tileBase)
        {
            Prefab = prefab;
            Instance = instance;
            Position = position;
            TileBase = tileBase;
        }
    }

    void Start()
    {
        tilemap = GetComponent<Tilemap>();
    }

    public void Spawn(GameObject prefab, Vector3Int position, Quaternion rotation, TileBase tileBase)
    {
        if (Application.isPlaying) return;

        GOTile existing = prefabs.Find(cand => cand.Position == position);
        if (existing != null && existing.Prefab != prefab)
        {
            DestroyImmediate(existing.Instance);
            prefabs.Remove(existing);
            existing = null;
        }
        if (existing == null)
        {
#if  !UNITY_EDITOR
            GameObject instance = Instantiate(prefab);
#else
            GameObject instance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
#endif
            instance.transform.position = position + anchor;
            instance.transform.rotation = rotation;
            instance.transform.parent = transform;
            prefabs.Add(new GOTile(prefab, instance, position, tileBase));
        }
    }


    void Update()
    {
        if (Application.isPlaying) return;

        prefabs.RemoveAll(
            tile =>
            {
                TileBase dummy = tilemap.GetTile(tile.Position);

                if (!dummy || dummy != tile.TileBase)
                {
                    DestroyImmediate(tile.Instance);
                    return true;
                }
                return false;
            }
        );
    }
}
